import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MakeWishRoutingModule } from './make-wish-routing.module';
import { MakeWishComponent } from './make-wish.component';


@NgModule({
  declarations: [MakeWishComponent],
  imports: [
    CommonModule,
    MakeWishRoutingModule
  ]
})
export class MakeWishModule { }
