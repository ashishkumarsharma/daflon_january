import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MakeWishComponent } from './make-wish.component';

const routes: Routes = [{ path: '', component: MakeWishComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MakeWishRoutingModule { }
