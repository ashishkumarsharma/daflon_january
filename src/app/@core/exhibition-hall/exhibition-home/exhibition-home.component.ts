import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { DataService } from 'src/app/services/data.service';
declare var $:any;
@Component({
  selector: 'app-exhibition-home',
  templateUrl: './exhibition-home.component.html',
  styleUrls: ['./exhibition-home.component.scss']
})
export class ExhibitionHomeComponent implements OnInit {
  ExhibitionList = [];
  hall_id;
  bgImg = 'assets/event/exhibition.jpg';
  totalStageHalls;
  page: number = 1;
  stall_list_rec = {};
  stall_id;
  pointers = [];
  constructor(private _ds:DataService, private router:Router, private _ar: ActivatedRoute) { }

  ngOnInit(): void {
    this._ar.paramMap.subscribe((params:Params)=>{
      this.hall_id = params.get('id');
      this.getExhibitionHallData(this.hall_id);
    });
  }

  getExhibitionHallData(id){
    this.page=1;
    if(this.page == 1){
      this.bgImg = 'assets/event/exhibition.jpg';
    }
    this._ds.getExhibitionFullHallsData().pipe(map((data:any)=>{
      const exhibitionData = data.result;      
      this.ExhibitionList = exhibitionData.filter(x=>x.id==this.hall_id);
      this.totalStageHalls = this.ExhibitionList[0]['stall_list'].length;
      this.stall_list_rec = this.ExhibitionList[0]['stall_list'][this.page - 1];
      let i = 0;
      this.stall_list_rec['stalls'].forEach(element => {
        i++;
        this.stall_list_rec['stalls'][i-1]['class']='p'+i;      
      });
    })).subscribe();
  }
  hallPaging(evt){
    if(evt === 'left'){
      $('#arrLeft').tooltip('hide');
      this.page--;
      this.bgImg = 'assets/event/exhibition.jpg';
      this.stall_list_rec = this.ExhibitionList[0]['stall_list'][this.page - 1];
      let i = 0;
      this.stall_list_rec['stalls'].forEach(element => {
        i++;
        this.stall_list_rec['stalls'][i-1]['class']='p'+i;      
      });
    }
    if(evt === 'right'){
      $('#arrRight').tooltip('hide');
      this.page++;
      this.bgImg = 'assets/event/exhibition2.jpg';
      this.stall_list_rec = this.ExhibitionList[0]['stall_list'][this.page - 1];
      let i = 0;
      this.stall_list_rec['stalls'].forEach(element => {
        i++;
        this.stall_list_rec['stalls'][i-1]['class']='p'+i;      
      });
    }
    // console.log(this.stall_list_rec)
  }

  pointerMethod(item){
    console.log(item);
    $(`.${item.class}`).tooltip('hide');

    this.router.navigate(['/exhibition/stall/'+item.id]);

  }
}
