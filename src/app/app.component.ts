// 

import { HttpClient } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router, RouterEvent } from '@angular/router';
import { delay } from 'rxjs/operators';
import { DataService } from './services/data.service';
import { LoaderService } from './services/loader.service';

declare var $:any;
declare let ga: Function;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit{
  title(title: any) {
    throw new Error('Method not implemented.');
  }
  isLoaded: boolean = false;
  route;
  landscape = true;
  bad: string;
 
  constructor(public _router: Router,private _loading: LoaderService, private http: HttpClient, private router: Router,private _fd: DataService){
    this.route = this.router;
    this.router.events.subscribe((e : RouterEvent) => {
      this.navigationInterceptor(e);
    })
  }

  ngOnInit(){
    this._router.events.subscribe((event:RouterEvent)=> {
      if(event instanceof NavigationStart){
        //alert(event.url);
        this.bad = event.url

        if (window.innerHeight > window.innerWidth && this.bad !== '/login') {
          this.landscape = false;
        }
        else{
          this.landscape = true;
        }

        ga('set', 'page', event.url);
        ga('send', 'pageview');
      }
     });

    this.listenToLoading();
    $(document).ready(()=> {
      $("body").tooltip({ selector: '[data-bs-toggle=tooltip]'});
    });
    if (window.innerHeight > window.innerWidth) {
      this.landscape = false;
    }
  }

  listenToLoading(): void {
    this._loading.loadingSub
      .pipe(delay(0)) 
      .subscribe((loading) => {
        this.isLoaded = loading;
      });
  }

  navigationInterceptor(event: RouterEvent): void {
    if (event instanceof NavigationStart) {
      this.isLoaded = true;
    }
    if (event instanceof NavigationEnd) {
      this.isLoaded = false;
    }

    if (event instanceof NavigationCancel) {
      this.isLoaded = false;
    }
    if (event instanceof NavigationError) {
      this.isLoaded = false;
    }
  }

  // @HostListener('window:resize', ['$event']) onResize(event) {
  //   if (window.innerHeight > window.innerWidth) {
  //     this.landscape = false;
  //   } else{
  //     this.landscape = true;
  //   }
  // }
  @HostListener('window:resize', ['$event']) onResize(event) {
    if (window.innerHeight > window.innerWidth && this.bad !== '/login') {
      this.landscape = false;
    } else{
      this.landscape = true;
    }
  }
}